import os
import codecs
import datetime

import logging

#封装logging日志
class LogFile:
    #构造函数 fileName：文件名
    def __init__(self,fileName,level=logging.INFO):
        fh = logging.FileHandler(fileName)
        self.logger = logging.getLogger()
        self.logger.setLevel(level)
        formatter = logging.Formatter('%(asctime)s : %(message)s','%Y-%m-%d %H:%M:%S')
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)

    def WriteLog(self,message):
        self.logger.info(message)

    def WriteErrorLog(self,message):
        self.logger.setLevel(logging.ERROR)
        self.logger.error(message)