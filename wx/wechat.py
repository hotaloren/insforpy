from wxpy import *
import urllib.request
import json
import threading

# 杭州-余姚
url = 'https://kyfw.12306.cn/otn/leftTicket/queryA?leftTicketDTO.train_date=2018-10-01&' \
      'leftTicketDTO.from_station=HGH&leftTicketDTO.to_station=YYH&purpose_codes=ADULT'
# 南京-余姚
# url = "https://kyfw.12306.cn/otn/leftTicket/queryA?leftTicketDTO.train_date=2018-10-01
# &leftTicketDTO.from_station=NKH&leftTicketDTO.to_station=YYH&purpose_codes=ADULT"
bot = Bot(console_qr=True, cache_path="login.pkl")
# 年宝的聊天对象
nb = bot.friends().search("吾皇")[0]

# 机器人账号自身
myself = bot.self
# myself.send('hello, wechat')
dog = bot.groups().search("狗窝")[0]

# dog.send('Hello, WeChat!')

# 向文件传输助手发送消息
# bot.file_helper.send('Hello from wxpy!')


@bot.register(dog)
def my_dog_friend_message(msg):
    print('[接收]' + str(msg))
    if msg.type == 'Text':   # 除文字外其他消息回复内容
        if msg.text == "嗨啊":
            return '嗨尼玛呢'
    elif "你来自哪里" in str(msg):   # 特定问题回答
        ret = "我来自鱼之家"
    else:         # 文字消息自动回答
        ret = ""

# 进入交互式的 Python 命令行界面，并堵塞当前线程
embed()

# nb.send('Hello, WeChat!')
# print(my_friend)
# <Friend: 游否>
