import urllib.request
from bs4 import BeautifulSoup
from database import MySQLCommand as db
import time
from log import LogFile as dolog

def crawlHtml(url):
    request = urllib.request
    response = request.urlopen(url)
    data = response.read().decode('utf-8')# 设置解码方式
    return BeautifulSoup(data,"lxml")

class db_video_sparide(object):

    def __init__(self,t_id):
        self.domain = 'https://movie.douban.com/'
        self.module = 'subject/'

        self.tid = t_id
        self.log = dolog(t_id+"_log.log")
        self.url = self.domain + self.module + self.tid+'/'


        self.start = 0
        self.limit = 20
    def do_sparide(self,type):

        self.url += type+'?start='+self.start+'&limit'+self.limit
        crawlHtml(self.url)

        self.start += self.limit



