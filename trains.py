import urllib.request
import json
import threading
url = "https://kyfw.12306.cn/otn/leftTicket/queryA?leftTicketDTO.train_date=2018-10-01&leftTicketDTO.from_station=NKH&leftTicketDTO.to_station=YYH&purpose_codes=ADULT"

# 请求
# 爬取结果
def sendTransInfo():
    print(1)
    requestObj = urllib.request
    response = requestObj.urlopen(url)
    data = response.read().decode('utf-8')
    # 设置解码方式
    result = json.loads(data)
    if result:
        print(result)
        station = [v for v in [item.split("|") for item in result["data"]["result"]] if "T" not in v[3] and "K" not in v[3]]
        infostrlist = ["杭州-余姚"]
        for info in station:
            if info[0] != "":
                if info[-7] != "" and info[-7] != "无":
                    infostr = "车次 ： " + str(info[3]) + "-二等座：" + str(info[-7]) + '行程: ' + info[10]
                    infostrlist.append(infostr)
                else:
                    infostr = "车次 ： " + str(info[3]) + "-一等座：" + str(info[-6]) + '|商务 ： ' + str(info[-5]) + '行程: ' + \
                              info[10]
                    infostrlist.append(infostr)
    # 嵌套定时任务，隔段时间执行
    # sendtimer = threading.Timer(20, sendTransInfo)
    # sendtimer.start()
timer = threading.Timer(1, sendTransInfo)
timer.start()

'''
print(data)
'''


print("抓取完成")
