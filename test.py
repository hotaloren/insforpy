import urllib.request
from bs4 import BeautifulSoup
from database import MySQLCommand as db
import time
nethttp = "http"
domain = "http://www.quanmama.com"
url = "http://www.quanmama.com/new"
key = '肯'

# 请求
# 爬取结果
def crawlHtml(url):
    request = urllib.request
    response = request.urlopen(url)
    data = response.read().decode('utf-8')# 设置解码方式
    return BeautifulSoup(data,"lxml")
data = crawlHtml(url)
# 打印结果
#print(data)

aList = data.find_all('a')
hrefList = [[v.get("href"),v.get("title")] for v in aList if v.get("title") != None and v.get("title").find(key) != -1]
htmlList = []
DB = db()
if len(hrefList):
    htmlList = [crawlHtml(item[0]) for item in hrefList if item != ""]

couponList = []
for v in htmlList:
    item = v.select("div.dataOne")
    if len(item):
        for i in item:
            couponTitle = i.find("div",{"class":"dataImagesText"})
            couponImg = i.find("div",{"class":"dataImg"})
            name = couponTitle.next.next.string
            imgurl = couponImg.next.next.next.next.get("src")
            if nethttp in couponTitle.next.next.get("href"):
                linkurl = couponTitle.next.next.get("href")
            else:
                linkurl = domain+couponTitle.next.next.get("href")

            #print(int(time.time()))
            DB.insertMysql([name,imgurl,linkurl,int(time.time())])
print("抓取完成")

# 打印爬取网页的各类信息
#print(type(response))
#print(response.geturl())
#print(response.info())
#print(response.getcode())